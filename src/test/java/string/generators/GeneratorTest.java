package string.generators;

import org.junit.jupiter.api.Test;
import string.generators.Generator;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class GeneratorTest {

    @Test
    public void testRandomNameHasAFirstNameAndALastName()  {
        String name =  Generator.randomName();
        String[] splitFirstAndLast = name.split(" ");
        assertTrue(splitFirstAndLast.length>=2);
    }

    @Test
    public void testRandomNameHasAFirstNameStartsWithUpperCaseAndLastNameStartsWithUppercase(){
        String name =  Generator.randomName();
        String[] splitFirstAndLast = name.split(" ");
        String first = ""+splitFirstAndLast[0].charAt(0);
        assertEquals(first.toUpperCase(),first);
        String firstOfLast = ""+splitFirstAndLast[1].charAt(0);
        assertEquals(firstOfLast.toUpperCase(),firstOfLast);
    }

    @Test
    public void testRandomNameHasAFirstNameEndsLowerCaseLettersAndLastNameEndsWithLowerCaseLetters(){
        String name =  Generator.randomName();
        String[] splitFirstAndLast = name.split(" ");
        String first = ""+splitFirstAndLast[0].substring(1);
        System.out.println(first);
        assertEquals(first.toLowerCase(),first);
        String firstOfLast = ""+splitFirstAndLast[1].substring(1);
        System.out.println(firstOfLast);
        assertEquals(firstOfLast.toLowerCase(),firstOfLast);
    }

    @Test
    public void testEmail_SuffixIsRight(){
        String emailSuffix = Generator.EMAIL_SUFFIX;
        assertEquals(emailSuffix, "@realdolmen.com");
    }

    @Test
    public void testRandomEmailGeneratorContainsPointComAndPiont(){
        String name =  Generator.randomEmailGenerator();
        assertTrue(name.contains(".com") && name.contains("."));
        if (name.contains(".com") && name.contains(".")){
            System.out.println("contains .com and .");;
        }else{
            System.out.println("Does not contains .com or .");
        }
    }

    @Test
    public void testRandomEmailGeneratorContainsAt(){
        String name =  Generator.randomEmailGenerator();
        assertTrue(name.contains("@"));
        if (name.contains("@")){
            System.out.println("contains @");;
        }else{
            System.out.println("Does not contains @");
        }
    }
}
