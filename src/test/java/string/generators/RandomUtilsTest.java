package string.generators;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

public class RandomUtilsTest {

    @Test
    public void testRandomIntLowerThanMaxAndgreaterThanMin() {
        int x = RandomUtils.getRandomInt(0, 5);
        assertTrue((x < 5) && (x >= 0));
    }

    @Test
    public void testRandomIntEqualToMin() {
        int x = RandomUtils.getRandomInt(6, 7);
        assertEquals(6, x);
    }

    @Test
    public void testGetRandomFirstNameIsElementOfArrayFirstNames() {
        String first = RandomUtils.getRandomFirstName();
        String test = Arrays.stream(RandomUtils.getFirstNames()).filter(s -> first.equals(s)).findFirst().get();
        assertEquals(test, first);
    }

    @Test
    public void testGetRandomLastName() {
        String last = RandomUtils.getRandomLastName();

//        String test = Arrays.stream(RandomUtils.getLastNames()).filter(s -> last.equals(s)).findFirst().get();
        String test = "";
        for(String s : RandomUtils.getLastNames()){
            if(s.equals(last)){
                test = s;
            }
        }
        assertEquals(test,last);

    }

    @Test
    public void testGetFirstNameIsNotNullOrEmpty() {
        String first = RandomUtils.getRandomFirstName();
        assertFalse(first == null || first.isEmpty());
    }

    @Test
    public void testGetLastNameIsNotNullOrEmpty() {
        String first = RandomUtils.getRandomLastName();
        assertFalse(first == null || first.isEmpty());
    }

    @Test
    public void testGetRandomIntThrowsNewRandomOutOfBoundsException() {
        int max = 8;
        assertAll((Executable) () -> {
            assertThrows(IllegalArgumentException.class, () -> {
                RandomUtils.getRandomInt(7, 7);
            });
            assertThrows(IllegalArgumentException.class, () -> {
                RandomUtils.getRandomInt(18, 7);
            });
        });
    }

    @Test
    public void testGetRandomIntBetween2Numbers() {
        int x = RandomUtils.getRandomInt(5, 7);
        assertTrue((x == 5) || (x == 6));
    }


    @Test
    public void testGetRandomFirstNameLowerCase() {
        String first = RandomUtils.getRandomFirstNameLowerCase();
        String nameAangepast;
        nameAangepast = first.toLowerCase();
        System.out.println(first);
        System.out.println(nameAangepast);

        assertEquals(first, nameAangepast);
    }

    @Test
    public void testGetRandomLastNameLowerCase() {
        String last = RandomUtils.getRandomLastNameLowerCase();
        String nameAangepast;
        nameAangepast = last.toLowerCase();
        System.out.println("Originele naam: " + last);
        System.out.println("Aangepaste naam: " + nameAangepast);

        assertEquals(last, nameAangepast);
    }


}
