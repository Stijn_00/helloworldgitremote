package string.generators;

import java.util.concurrent.ThreadLocalRandom;

public class RandomUtils {

    private static final String[] FIRST_NAMES = {"Tim", "Yoni", "Maxime", "Jeroen", "Stijn", "Seda", "Alexander", "Arne", "Steven"};
    private static final String[] LAST_NAMES = {"Van Caekenberg", "Vindelinckx", "Buelens", "Hesters", "Gul", "Coeckelbergh","Esnol", "Wauters", "De Cock"};

    static String[] getFirstNames() {
        return FIRST_NAMES;
    }

    static String[] getLastNames() {
        return LAST_NAMES;
    }

    public static String getRandomFirstName() {
        return FIRST_NAMES[getRandomInt(0, FIRST_NAMES.length)];
    }

    public static String getRandomLastName() {
        return LAST_NAMES[getRandomInt(0, LAST_NAMES.length)];
    }


    public static int getRandomInt(int min, int max) {
        return ThreadLocalRandom.current().nextInt(min, max);

    }

    public static String getRandomFirstNameLowerCase(){
        return getRandomFirstName().toLowerCase();
    }

    public static String getRandomLastNameLowerCase(){
        return getRandomLastName().toLowerCase();
    }

    public static String getRandomFirstNameLowerCaseUnderscore(){
        if(getRandomFirstNameLowerCase().contains(" ")){
            String firstNameReplacement;
            firstNameReplacement = getRandomFirstNameLowerCase().replaceAll(" ", "_");
            return firstNameReplacement;
        }
        else{
            return getRandomFirstNameLowerCase();
        }
    }

    public static String getRandomLastNameLowerCaseUnderscore(){
        if(getRandomLastNameLowerCase().contains(" ")){
            String lastNameReplacement;
            lastNameReplacement = getRandomLastNameLowerCase().replaceAll(" ", "_");
            return lastNameReplacement;
        }
        else{
            return getRandomLastNameLowerCase();
        }
    }
}
