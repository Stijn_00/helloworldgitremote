package string.generators;

public class Generator {

    public static String randomName()  {
        return RandomUtils.getRandomFirstName()+" "+RandomUtils.getRandomLastName();
    }
    public static String randomEmailGenerator() {
        return RandomUtils.getRandomFirstNameLowerCaseUnderscore()+ "." + RandomUtils.getRandomLastNameLowerCaseUnderscore() + Generator.EMAIL_SUFFIX; }
    public static final String EMAIL_SUFFIX = "@realdolmen.com";

    private static String stripSpaces(String last){
        StringBuilder builder = new StringBuilder();
        String[] lastNames = last.split(" ");
        for(String s : lastNames){
            builder.append(s.toLowerCase());
        }
        return builder.toString();
    }
}
